
{
  "name": "Docker Compose",
  "description": "Deploy your application with [Docker Compose](https://docs.docker.com/compose/)",
  "template_path": "templates/gitlab-ci-docker-compose.yml",
  "kind": "hosting",
  "prefix": "dcmp",
  "job_prefix": "compose",
  "is_component": true,
  "variables": [
    {
      "name": "DCMP_IMAGE",
      "description": "The Docker image used to run Docker Compose CLI commands - **set the version required by your Docker Compose cluster**",
      "default": "registry.hub.docker.com/library/docker:latest"
    },
    {
      "name": "DCMP_CMD",
      "description": "The docker compose command (empty means _auto_)",
      "values": ["", "docker compose", "docker-compose"],
      "advanced": true
    },
    {
      "name": "DCMP_BASE_APP_NAME",
      "description": "Base application name",
      "default": "$CI_PROJECT_NAME",
      "advanced": true
    },
    {
      "name": "DCMP_ENVIRONMENT_URL",
      "type": "url",
      "description": "The default environments url _(only define for static environment URLs declaration)_\n\n_supports late variable expansion (ex: `https://%{environment_name}.dcmp.acme.com`)_"
    },
    {
      "name": "DCMP_SCRIPTS_DIR",
      "description": "Directory where Compose files, dotenv files and hook scripts are located",
      "default": ".",
      "advanced": true
    },
    {
      "name": "DCMP_UP_OPTS",
      "description": "[`compose up` options](https://docs.docker.com/reference/cli/docker/compose/up/#options)",
      "default": "--no-build --remove-orphans --wait --wait-timeout 180"
    },
    {
      "name": "DCMP_DOWN_OPTS",
      "description": "[`compose down` options](https://docs.docker.com/reference/cli/docker/compose/down/#options)",
      "default": "--volumes --remove-orphans --rmi all"
    },    
    {
      "name": "DCMP_SSH_PRIVATE_KEY",
      "description": "Default SSH key to use when connecting to Docker hosts over SSH (can be overridden per env)",
      "secret": true
    },
    {
      "name": "DCMP_SSH_KNOWN_HOSTS",
      "description": "SSH `known_hosts` (file or text variable)"
    }
],
  "features": [
    {
      "id": "config",
      "name": "Compose Config",
      "description": "Runs [`compose config`](https://docs.docker.com/reference/cli/docker/compose/config/) to detect errors in your Compose file(s)",
      "disable_with": "DCMP_CONFIG_DISABLED",
      "variables": [
        {
          "name": "DCMP_CONFIG_OPTS",
          "description": "[`compose config` options](https://docs.docker.com/reference/cli/docker/compose/config/#options)",
          "default": "--quiet",
          "advanced": true
        }
      ]
    },
    {
      "id": "review",
      "name": "Review",
      "description": "Dynamic review environments for your topic branches (see GitLab [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/))",
      "variables": [
        {
          "name": "DCMP_REVIEW_DOCKER_HOST",
          "description": "Docker Host for `review` env (ex: `ssh://docker@docker-host-for-review:2375`)",
          "mandatory": true
        },
        {
          "name": "DCMP_REVIEW_SSH_PRIVATE_KEY",
          "description": "`review` env specific SSH key to use when connecting to Docker Host over SSH",
          "secret": true
        },
        {
          "name": "DCMP_REVIEW_APP_NAME",
          "description": "The application name for `review` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "DCMP_REVIEW_AUTOSTOP_DURATION",
          "description": "The amount of time before GitLab will automatically stop `review` environments",
          "default": "4 hours"
        },
        {
          "name": "DCMP_REVIEW_ENVIRONMENT_URL",
          "type": "url",
          "description": "The `review` environments url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        }
      ]
    },
    {
      "id": "integration",
      "name": "Integration",
      "description": "A continuous-integration environment associated to your integration branch (`develop` by default)",
      "variables": [
        {
          "name": "DCMP_INTEG_DOCKER_HOST",
          "description": "Docker Host for `integration` env (ex: `ssh://docker@docker-host-for-integ:2375`)",
          "mandatory": true
        },
        {
          "name": "DCMP_INTEG_SSH_PRIVATE_KEY",
          "description": "`integration` env specific SSH key to use when connecting to Docker Host over SSH",
          "secret": true
        },
        {
          "name": "DCMP_INTEG_APP_NAME",
          "description": "The application name for `integration` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "DCMP_INTEG_ENVIRONMENT_URL",
          "type": "url",
          "description": "The `integration` environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        }
      ]
    },
    {
      "id": "staging",
      "name": "Staging",
      "description": "An iso-prod environment meant for testing and validation purpose on your production branch (`main` or `master` by default)",
      "variables": [
        {
          "name": "DCMP_STAGING_DOCKER_HOST",
          "description": "Docker Host for `staging` env (ex: `ssh://docker@docker-host-for-staging:2375`)",
          "mandatory": true
        },
        {
          "name": "DCMP_STAGING_SSH_PRIVATE_KEY",
          "description": "`staging` env specific SSH key to use when connecting to Docker Host over SSH",
          "secret": true
        },
        {
          "name": "DCMP_STAGING_APP_NAME",
          "description": "The application name for `staging` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "DCMP_STAGING_ENVIRONMENT_URL",
          "type": "url",
          "description": "The `staging` environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        }
      ]
    },
    {
      "id": "prod",
      "name": "Production",
      "description": "The production environment",
      "variables": [
        {
          "name": "DCMP_PROD_DOCKER_HOST",
          "description": "Docker Host for `production` env (ex: `ssh://docker@docker-host-for-prod:2375`)",
          "mandatory": true
        },
        {
          "name": "DCMP_PROD_SSH_PRIVATE_KEY",
          "description": "`production` env specific SSH key to use when connecting to Docker Host over SSH",
          "secret": true
        },
        {
          "name": "DCMP_PROD_APP_NAME",
          "description": "The application name for `production` env (only define to override default)",
          "advanced": true
        },
        {
          "name": "DCMP_PROD_ENVIRONMENT_URL",
          "type": "url",
          "description": "The `production` environment url _(only define for static environment URLs declaration and if different from default)_",
          "advanced": true
        },
        {
          "name": "DCMP_PROD_DEPLOY_STRATEGY",
          "description": "Defines the deployment to `production` strategy.",
          "type": "enum",
          "values": ["manual", "auto"],
          "default": "manual"
        }
      ]
    }
  ]
}
