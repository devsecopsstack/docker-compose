## [1.0.2](https://gitlab.com/to-be-continuous/docker-compose/compare/1.0.1...1.0.2) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([bedff8b](https://gitlab.com/to-be-continuous/docker-compose/commit/bedff8bbf87b5e77e66cbd34fdc08103f2cac8e5))

## [1.0.1](https://gitlab.com/to-be-continuous/docker-compose/compare/1.0.0...1.0.1) (2024-04-27)


### Bug Fixes

* export COMPOSE_FILE variable ([4135b1c](https://gitlab.com/to-be-continuous/docker-compose/commit/4135b1cd874ca94234b3f32a93875d93df00dafa))

# 1.0.0 (2024-04-09)


### Features

* initial version ([df08495](https://gitlab.com/to-be-continuous/docker-compose/commit/df0849597058a7594093f7ce2e2d4d302891205b))
